import { w3cwebsocket } from 'websocket';
import { exit } from 'process';
import * as CryptoJS from 'crypto-js';
import { pack } from '@/utils/transfer';
import { randomUUID } from 'crypto';
import { EventEmitter } from 'stream';
import debug from 'debug';
import { IWebSocketClient } from './types';

const dMsg = debug('client');

interface EventData<T = any> {
    requestToken?: string;
    eventType: string;
    result: T;
}

type Fn = (...args: any[]) => any;

export class WebSocketClient extends w3cwebsocket implements IWebSocketClient {
    public evCount = new Map<string, number>();
    public evPool: EventEmitter;
    public algo: 'aes' | 'hmac';
    public secret: string = '';
    public timeoutMilli: number = 3000;
    public hstSize: number = 30;
    public records : string[] = [];

    constructor(url: string, algo: 'aes' | 'hmac') {
        super(url);
        this.evPool = new EventEmitter();
        this.algo = algo;

        this.onopen = () => {
            dMsg('CLIENT CONNECTED!!!!!!!!!!');
            // TODO
            this.send(this.algo);
        };

        this.onclose = () => {
            dMsg('GOOD BYE');
            exit(0);
        };

        this.initial();
    }

    dispatch(eventType: string, payload?: any): void {
        this.evPool.emit(eventType, payload);
    }

    on(eventType: string, listener: Fn): number {
        // TODO
        this.evPool.on(eventType,listener);
        if(this.evCount.has(eventType) === false)
        {
            this.evCount.set(eventType,0);
        }
        this.evCount.set(eventType,  this.evCount.get(eventType)! + 1 );
        return this.evCount.get(eventType)!;
    }

    once(eventType: string, listener: Fn): number {
        // TODO
        this.evPool.once(eventType,listener);
        if(this.evCount.has(eventType) === false)
        {
            this.evCount.set(eventType,0);
        }
        this.evCount.set(eventType,  this.evCount.get(eventType)! + 1 );
        return this.evCount.get(eventType)!;
    }

    off(eventType: string): boolean {
        // TODO
        if(this.evCount.has(eventType) === false)
        {
            return false;
        }
        if(this.evCount.get(eventType) === 0)
        {
            return false;
        }
        this.evPool.removeAllListeners(eventType);
        this.evCount.set(eventType,0);
        return true;
    }

    async ready() {
        // TODO
        return new Promise<string>((resolve) => 
        {
            this.onmessage = ({ data }) => {
                resolve(data as string);
            };
        });
    }

    async initial() {
        this.secret = await this.ready();

        this.onmessage = ({ data }) => {
            dMsg('Receive Raw%o', data);
            // TODO
            try {
                const receiveData = JSON.parse(data as string) as EventData;
                if(typeof receiveData.requestToken === 'string')
                {
                    if(receiveData.requestToken !== '')
                    {
                        this.dispatch(receiveData.requestToken, receiveData.result);
                    }
                }
                else
                {
                    this.dispatch(receiveData.eventType, receiveData.result);
                }
                dMsg('Resolve : ');
                dMsg(receiveData);
            } catch (error) {
                dMsg(`Error : ${error}`);
            }
        };
    }

    async sendout(payload: any, requestToken: string = '$NONE') {
        // TODO
        let sendData = { 
            requestToken : requestToken,
            signature : '',
            payload: payload
        };
        switch (this.algo) {
            case 'aes':
                sendData.signature = CryptoJS.AES.encrypt(requestToken, this.secret).toString();
                break;
            case 'hmac':
                sendData.signature = CryptoJS.HmacSHA1(requestToken, this.secret).toString();
                break;
            default:
                return;
        }
        this.send(pack(sendData));
    }

    async request(eventType: string, params?: any): Promise<any> {
        const requestToken = `request::${randomUUID()}`;
        // TODO
        let payload = { 
            eventType : eventType,
            params : params 
        };
        this.sendout(payload, requestToken);

        return new Promise((resolve) => {
            // TODO
            this.on(requestToken, (response) => {
                let record = `[${response.message}] ${response.command}`;
                this.records.push(record);
                if(this.records.length > this.hstSize)
                {
                    this.records.shift();
                }
                this.off(requestToken);
                resolve("RESOLVE");
            });
            setTimeout(() => { resolve("TIMEOUT"); }, this.timeoutMilli);
        });
    }

    history() {
        // TODO
        dMsg(this.records);
        return this.records;
    }
}
